import math
class constants():
    pi = 3.14159265
    wheelSpacing = 18.0
    fieldSizeX = 144.0
    fieldSizeY = 144.0
c = constants()
def calculateRotationDegrees(leftArcLen, rightArcLen):
    #X * (l - r) = 53.407075111
    #X = 53.407075111 / (l - r)
    #d = 360 / X
    if(leftArcLen == rightArcLen):
        modifier = 0.0
        return 0.0
    elif(leftArcLen > rightArcLen):
        a = leftArcLen
        b = rightArcLen
        modifier = 1.0
    elif(leftArcLen < rightArcLen):
        a = rightArcLen
        b = leftArcLen
        modifier = -1.0
    X = (c.pi * c.wheelSpacing) / (float(a) - float(b))
    return modifier * ((360.0/X))

def midArc(l, r):
    return (l + r) / 2.0

def atcfDist(l, r):
    #Chord length = 2 * radius * sin(angle / 2)
    degs = abs(calculateRotationDegrees(l,r)) % 360
    arcDist = midArc(l,r)
    if degs != 0:
        multiplier = 360 / degs
    else:
        return arcDist
    radius = (arcDist * multiplier) / (2 * c.pi)

    chordLen = 2 * radius * math.sin(math.radians(degs) / 2)

    return chordLen


#Test area:
roPos = {
    "x" : 0, #X coordinate
    "y" : 0, #Y coordinate
    "z" : 0  #Facing direction (in degrees) (0 = facing up X-axis + )
}
for x in range(-5,5):
    for y in range (-5,5):
        #print(calculateRotationDegrees(x,y))
        #print(midArc(x,y))
        #print(atcfDist(x,y))

        rx = roPos["x"]
        ry = roPos["y"]
        rz = roPos["z"]

        roPos["z"] = (rz + calculateRotationDegrees(x,y)) % 360

        middeg = (rz + (roPos["z"])) / 2
        chord = atcfDist(x,y)

        if(middeg == 0 or middeg == 180):
            roPos["x"] += midArc(x,y)
        elif(middeg == 90 or middeg == 270):
            roPos["y"] += midArc(x,y)
        else:
            dx = math.cos(middeg) * chord
            dy = math.sin(middeg % 90) * chord
            roPos["x"] += dx
            roPos["y"] += dy

        print(roPos)
